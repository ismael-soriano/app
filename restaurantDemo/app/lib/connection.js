(function() {
	var start = null,
	    serviceURL,
	    _settings = {
		request : {
			url : Alloy.Globals.serverURL,
			accept : 'application/json',
			contentType : 'application/x-www-form-urlencoded'
		}
	};

	function Connection(_parameters, _callback) {
		this._parameters = _parameters;
		this._callback = _callback;
		this.response = createResponseBasedOnParameters(this._parameters, this._callback);
		this.client = createClient(this.response, this._callback);

		this.send = function() {
			if (this.response.requestParams !== null && this.response.requestType === 'GET') {
				this.client.send(this.response.requestParams);
			} else if (this._parameters.file !== undefined && this._parameters.file !== null) {
				this.client.send(this._parameters.file);
			} else {
				if (this.response.requestParams !== null) {
					this.client.send(this.response.requestParams);
				} else {
					this.client.send();
				}
			}
		};
	}

	function createResponseBasedOnParameters(_parameters, _callback) {
		var response = {
			requestType : 'POST',
			contentType : _settings.request.contentType,
			boundary : null,
			type : null,
			enctype : null,
			serviceName : '',
			serviceToken : null,
			authentication : null,
			callback : null,
			status : 'error',
			statusCode : null,
			errorText : '',
			responseText : '',
			rows : [],
			requestParams : null
		};

		// Service Options
		if (_parameters === undefined || _parameters === null || typeof (_parameters) !== 'object') {
			Ti.API.warn('Error Service Response 0001: Invalid Parameters to invokeService.');
			response.errorText = 'Invalid Parameters to invokeService.';
			response.status = 'error';

			executeCallback(_callback, response);
		}

		// Request Type
		if (_parameters.requestType !== undefined && _parameters.requestType !== null && typeof (_parameters.requestType) === 'string')
			response.requestType = _parameters.requestType;

		// Content Type
		if (_parameters.contentType !== undefined && _parameters.contentType !== null && typeof (_parameters.contentType) === 'string')
			response.contentType = _parameters.contentType;

		// Service Token Enum
		if (_parameters.serviceToken !== undefined && _parameters.serviceToken !== null && typeof (_parameters.serviceToken) === 'string')
			response.serviceToken = _parameters.serviceToken;

		// Encoding Type
		if (_parameters.enctype !== undefined && _parameters.enctype !== null && typeof (_parameters.enctype) === 'string')
			response.enctype = _parameters.enctype;

		// Type
		if (_parameters.type !== undefined && _parameters.type !== null && typeof (_parameters.type) === 'string')
			response.type = _parameters.type;

		// Boundary
		if (_parameters.boundary !== undefined && _parameters.boundary !== null && typeof (_parameters.boundary) === 'string')
			response.boundary = _parameters.boundary;

		// Service Address
		if (_parameters.serviceName !== undefined && _parameters.serviceName !== null && typeof (_parameters.serviceName) === 'string') {
			response.serviceName = _parameters.serviceName;
			setSetviceURL(_parameters);
		} else {
			Ti.API.warn('Error Service Response 0002: Invalid Service Method.');
			response.errorText = 'Invalid Service Method.';
			response.status = 'error';

			executeCallback(_callback, response);
		}

		// Service Parameters
		if (_parameters.postParams !== undefined && _parameters.postParams !== null)
			response.requestParams = _parameters.postParams;

		// Service Callback
		if (_parameters.callback !== undefined && _parameters.callback !== null && typeof (_parameters.callback) === 'function')
			response.callback = _parameters.callback;

		// Service Authentication
		if (_parameters.authentication !== undefined && _parameters.authentication !== null && typeof (_parameters.authentication) === 'object')
			response.authentication = _parameters.authentication;

		if (_parameters.callbackParams !== undefined && _parameters.callbackParams !== null)
			response.callbackParams = _parameters.callbackParams;

		return response;
	}

	function setSetviceURL(_parameters) {
		if (_parameters.baseURL !== undefined && _parameters.baseURL !== null && typeof (_parameters.baseURL) === 'string') {
			serviceURL = _parameters.baseURL + _parameters.serviceName;
		} else {
			serviceURL = _settings.request.url + _parameters.serviceName;
		}
	}

	function createClient(response, _callback) {
		var client = Ti.Network.createHTTPClient();
		client.open(response.requestType, serviceURL);
		client.connectionType = response.requestType;
		client.setRequestHeader('Accept', _settings.request.accept);
		if (response.contentType !== 'none') {
			if (response.requestType === 'GET')
				response.contentType = 'application/json';

			client.setRequestHeader('Content-Type', response.contentType);
		}

		if (response.enctype !== null)
			client.setRequestHeader('enctype', response.enctype);

		if (response.type !== null)
			client.setRequestHeader('type', response.type);

		if (response.boundary !== null)
			client.setRequestHeader('boundary', response.boundary);

		client.setRequestHeader('Authorization', 'Basic ' + response.authentication);
		client.setTimeout(120000);

		client.onload = function() {
			response.statusCode = this.status;
			response.responseText = this.responseText;

			if (this.status >= 200 && this.status < 300) {
				response.errorText = '';
				response.status = 'success';

				try {
					response.rows = JSON.parse(this.responseText);
				} catch(ex) {
					response.rows = [];
				}

			} else {
				response.errorText = this.responseText;
				response.status = 'error';
				Ti.API.warn('Error Service Response 0003: ' + this.status + ' - ' + this.responseText);
			}

			executeCallback(_callback, response);
			var end = new Date().getTime();
			Ti.API.info('Conection onLoad elapsed ms: ' + (end - start));
		};

		// Manage errors
		client.onerror = function() {
			Ti.API.warn('Error Service Response 0004: ' + this.status + ' - ' + this.responseText);
			response.errorText = this.responseText;
			response.status = 'error';
			response.statusCode = this.status;

			executeCallback(_callback, response);
			var end = new Date().getTime();
			Ti.API.info('Conection onError elapsed ms: ' + (end - start));
		};

		return client;
	}

	function invokeService(_parameters, _callback) {
		start = new Date().getTime();
		var connection = new Connection(_parameters, _callback);
		connection.send();
	}

	function executeCallback(_callback, response) {
		try {
			if (_callback !== undefined && _callback !== null && typeof (_callback) === 'function') {
				if (response.callbackParams !== undefined && response.callbackParams !== null)
					_callback(response.callbackParams, response);

				_callback(response);
			}
		} catch (ex) {
			Ti.API.info(ex);
		}
	}

	exports.invoke = invokeService;
})();
