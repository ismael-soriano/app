/**
 * Compatibility Layer for the Titanium applications.
 *
 * @author Ismael Soriano
 */
ISCL = {
	ver : 0.2,

	/**
	 * Custom element creation to add a 100% compatible methods
	 * between android and IOS.
	 */
	UI : {
		getScreenWidth : function() {
			return (Ti.Platform.osname === 'android') ? (Titanium.Platform.displayCaps.xdpi * 2) : Ti.Platform.displayCaps.platformWidth;
		},

		getScreenHeight : function() {
			return (Ti.Platform.osname === 'android') ? (Ti.Platform.displayCaps.ydpi * 2) : Ti.Platform.displayCaps.platformHeight;
		},

		/**
		 * Changes the text of the object to the String referenced.<br><br>
		 *
		 * Usage: ISCL.UI.changeText(object, text);
		 *
		 * @param {Object} object - the object could be a Label or a button.
		 * @param {String} text - String containing the text to put in the object.
		 */
		changeText : function(object, text) {
			if (Ti.Platform == 'android') {
				object.setText(text);
			} else {
				object.text = text;
			}
		},

		/**
		 * Type of indicators for the Activity Indicator Style.
		 */
		indicatorType : {
			PLAIN : 'PLAIN',
			DARK : 'DARK',
			BIG : 'BIG'
		},

		/**
		 * Returns the style associated to the type passed as a parameter,
		 * if the parameter is null, then returns the basic style.
		 *
		 * @param {indicatorType} type
		 * @return {ActivityIndicatorStyle}
		 */
		getActivityIndicatorStyle : function(type) {
			if (type === null)
				type = 'PLAIN';

			switch(type) {
			case 'PLAIN':
				if (Ti.Platform.name === 'iPhone OS') {
					return Ti.UI.iPhone.ActivityIndicatorStyle.PLAIN;
				} else {
					return Ti.UI.ActivityIndicatorStyle.PLAIN;
				}
				break;

			case 'DARK':
				if (Ti.Platform.name === 'iPhone OS') {
					return Ti.UI.iPhone.ActivityIndicatorStyle.DARK;
				} else {
					return Ti.UI.ActivityIndicatorStyle.DARK;
				}
				break;

			case 'BIG':
				if (Ti.Platform.name === 'iPhone OS') {
					return Ti.UI.iPhone.ActivityIndicatorStyle.BIG;
				} else {
					return Ti.UI.ActivityIndicatorStyle.BIG;
				}
				break;
			}
		},

		/**
		 *
		 * @param {String} text
		 * @return {View}
		 */
		createConectionErrorView : function(_text) {
			var view = Ti.UI.createView({
				backgroundColor : 'white',
				layout : 'vertical'
			});

			var upperLabel = Ti.UI.createLabel({
				top : '50%',
				text : _text
			});

			var downLabel = Ti.UI.createLabel({
				text : 'Por favor verifica tu conexión'
			});

			view.add(upperLabel);
			view.add(downLabel);

			return view;
		},

		/**
		 *
		 * @param {Object} args
		 * @return {View}
		 */
		createLoadingPage : function(args) {
			var view = Ti.UI.createView({
				backgroundColor : 'black'
			}),

			    activityIndicator = Ti.UI.createActivityIndicator({
				indicatorColor : 'blue'
			});

			activityIndicator.setStyle(ISCL.UI.getActivityIndicatorStyle(ISCL.UI.indicatorType.BIG));
			activityIndicator.show();
			view.add(activityIndicator);

			return view;
		},

		/**
		 * Shows an alert dialog with the contents passed as parameters.
		 *
		 * @param {String} title
		 * @param {String} message
		 * @param {String} okMessage
		 */
		showAlertDialog : function(_title, _message, okMessage) {
			return Ti.UI.createAlertDialog({
				title : _title,
				message : _message,
				ok : okMessage
			}).show();
		},

		createAlertDialog : function(_title, _message, okMessage) {
			return Ti.UI.createAlertDialog({
				title : _title,
				message : _message,
				ok : okMessage
			});
		},
	},

	Colors : {
		GRAY_2 : '#504A4A',
		BLUE_GRAY : '#445B99',
		SILVER : '#DFDFDF'
	},

	/**
	 * Includes various functions to manage the intersctions between the appllication
	 * and the server.
	 */
	Utils : {

		/**
		 * Gets a bulk of data from the Server.<br><br>
		 *
		 * Usage: ISCL.serverConnection.getData(_callback);
		 *
		 * @param {Function} _callback
		 * @return {void}
		 */
		getData : function(_callback) {
			var token = Alloy.Globals.idENCUser + Alloy.Globals.idAppEnc + Alloy.Globals.version + Alloy.Globals.serverKey;
			var key = Titanium.Utils.md5HexDigest(token);

			var jsonParams = {
				idENCUser : Alloy.Globals.idENCUser,
				idAppEnc : Alloy.Globals.idAppEnc,
				version : Alloy.Globals.version,
				key : key
			};

			var jsonInvoke = JSON.stringify(jsonParams);
			require('connection').invoke({
				serviceName : Alloy.Globals.getDataService,
				postParams : {
					"data" : jsonInvoke
				}
			}, _callback);
		},

		/**
		 * Ensures that a user belongs to the database.<br><br>
		 *
		 * Usage: ISCL.ServerConnection.doLogin(usr, pwd, _callback);
		 *
		 * @param {Object} usr
		 * @param {Object} pwd
		 * @param {Object} _callback
		 */
		doLogin : function(usr, pwd, _callbackParams, _callback) {
			var token = Alloy.Globals.idENCUser + Alloy.Globals.idAppEnc + Alloy.Globals.version + Alloy.Globals.serverKey;
			var key = Titanium.Utils.md5HexDigest(token);

			Ti.API.info('User: ' + usr + ', password: ' + pwd);
			var jsonParams = {
				idENCUser : Alloy.Globals.idENCUser,
				idAppEnc : Alloy.Globals.idAppEnc,
				version : Alloy.Globals.version,
				user : usr,
				password : pwd,
				key : key,
			};

			var jsonInvoke = JSON.stringify(jsonParams);
			require('connection').invoke({
				serviceName : Alloy.Globals.loginService,
				callbackParams : _callbackParams,
				postParams : {
					"data" : jsonInvoke
				}
			}, _callback);
		},

		checkBooking : function(company, service, date, beginningHour, finalHour, _callbackParams, _callback) {
			var token = Alloy.Globals.idENCUser + Alloy.Globals.idAppEnc + Alloy.Globals.version + Alloy.Globals.serverKey;
			var key = Titanium.Utils.md5HexDigest(token);

			var jsonParams = {
				idENCUser : Alloy.Globals.idENCUser,
				idAppEnc : Alloy.Globals.idAppEnc,
				version : Alloy.Globals.version,
				company : company,
				service : service,
				day : date,
				entry : beginningHour,
				exit : finalHour,
				key : key
			};

			var jsonInvoke = JSON.stringify(jsonParams);
			require('connection').invoke({
				serviceName : Alloy.Globals.checkBookingService,
				callbackParams : _callbackParams,
				postParams : {
					"data" : jsonInvoke
				}
			}, _callback);
		},

		saveBooking : function(usr, company, service, beginningHour, exit, date, _callbackParams, _callback) {
			var token = Alloy.Globals.idENCUser + Alloy.Globals.idAppEnc + Alloy.Globals.version + Alloy.Globals.serverKey;
			var key = Titanium.Utils.md5HexDigest(token);

			var jsonParams = {
				idENCUser : Alloy.Globals.idENCUser,
				idAppEnc : Alloy.Globals.idAppEnc,
				version : Alloy.Globals.version,
				user : usr,
				company : company,
				service : service,
				entry : beginningHour,
				exit : exit,
				day : date,
				key : key,
			};

			var jsonInvoke = JSON.stringify(jsonParams);
			require('connection').invoke({
				serviceName : Alloy.Globals.doBookingService,
				callbackParams : _callbackParams,
				postParams : {
					"data" : jsonInvoke
				}
			}, _callback);
		},

		/**
		 * Checks the errors from the response Object recieved from the server.<br><br>
		 *
		 * Usage: ISCL.Utils.checkResponseError(response);
		 *
		 * @param {Object} response
		 */
		checkResponseError : function(response) {
			//Ti.API.info('_' + response.responseText + "_");
			if (response.status == 'error') {
				Ti.API.info('Error connecting to server');
				alert("Error al conectar con el servidor. Comprueba tu conexión o inténtalo más tarde");
				return -1;
			}
			if (response.responseText == null || response.responseText == '') {
				Ti.API.info('Error: null or "" received from server');
				return 1;
			}
			return 0;
		},

		isResponseOk : function(response) {
			if (response.status == 'error') {
				Ti.API.info('Error connecting to server');
				Ti.App.fireEvent('widget:dataError');
				alert("Error al conectar con el servidor. Comprueba tu conexión o inténtalo más tarde");
				return false;
			}
			if (response.responseText === null || response.responseText === '') {
				Ti.API.info('Error: null or "" received from server');
				return false;
			}
			
			return true;
		},

		/**
		 * Returns a String containing the data from the Json Object received.
		 *
		 * @param {Object} response
		 */
		parseJson : function(response) {
			var tmp = response.responseText.replace(/\"{/g, '{').replace(/\\"/g, '"').replace(/\}\"/g, '}').replace(/\\\\\//g, '/');
			Ti.API.info(tmp);
			return JSON.parse(tmp);
		},

		throwError : function(_message) {
			throw {
				message : _message
			};
		},

		isConnectionAvailable : function() {
			if (Titanium.Network.networkType === Titanium.Network.NETWORK_NONE) {
				return false;
			} else {
				return true;
			}
		}
	}
};
