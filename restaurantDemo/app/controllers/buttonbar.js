/**
 * @author Ismael
 */
var win = $.service_details;
var spacer =  ISCL.UI.getScreenWidth()*0.25;
var width = spacer-4;
var height = 36;

// TAB BAR
var tabBar = Ti.UI.createView({
    width: ISCL.UI.getScreenWidth(),
    height:40,
    left:0,
    bottom:0,
    backgroundColor:'#000'
});
win.add(tabBar);
// TAB 1
var tab1 = Ti.UI.createView({
    width:width,
    height:height,
    left:2,
    bottom:2,
    backgroundColor:'#333',
    borderRadius:2
});
var tab1Label = Ti.UI.createLabel({
    text:'Tab 1',
    color:'#FFF'
});
tab1.add(tab1Label);
tabBar.add(tab1);
// TAB 2
var tab2 = Ti.UI.createView({
    width:width,
    height:height,
    left:spacer,
    bottom:2,
    backgroundColor:'#000'
});
var tab2Label = Ti.UI.createLabel({
    text:'Tab 2',
    color:'#333'
});
tab2.add(tab2Label);
tabBar.add(tab2);
// TAB 3
var tab3 = Ti.UI.createView({
    width:width,
    height:height,
    left:(spacer*2),
    bottom:2,
    backgroundColor:'#000'
});
var tab3Label = Ti.UI.createLabel({
    text:'Tab 3',
    color:'#333'
});
tab3.add(tab3Label);
tabBar.add(tab3);
// TAB 4
var tab4 = Ti.UI.createView({
    width:width,
    height:height,
    left:(spacer*3),
    bottom:2,
    backgroundColor:'#000'
});
var tab4Label = Ti.UI.createLabel({
    text:'Tab 4',
    color:'#333'
});
tab4.add(tab4Label);
tabBar.add(tab4);
 
var currTab = tab1;
 
// ADD EVENT LISTENERS
tab1.addEventListener('click',function() {
    currTab.backgroundColor = '#000';
    currTab.children[0].color = '#333';
    this.backgroundColor = '#333';
    this.children[0].color = '#FFF';
    currTab = this;
});
tab2.addEventListener('click',function() {
    currTab.backgroundColor = '#000';
    currTab.children[0].color = '#333';
    this.backgroundColor = '#333';
    this.children[0].color = '#FFF';
    currTab = this;
});
tab3.addEventListener('click',function() {
    currTab.backgroundColor = '#000';
    currTab.children[0].color = '#333';
    this.backgroundColor = '#333';
    this.children[0].color = '#FFF';
    currTab = this;
 
    currTab.backgroundColor = '#000';
    this.backgroundColor = '#333';
    this.children[0].color = '#FFF';
    currTab = this;
});
tab4.addEventListener('click',function() {
    currTab.backgroundColor = '#000';
    currTab.children[0].color = '#333';
    this.backgroundColor = '#333';
    this.children[0].color = '#FFF';
    currTab = this;
});