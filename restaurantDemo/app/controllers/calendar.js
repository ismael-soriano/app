var args = arguments[0] || {};

Ti.API.info(args);

var moment = require('alloy/moment');
var currentMonth = moment();
var widget = Alloy.createWidget('jp.co.mountposition.calendar', 'widget', {
	min : currentMonth,
	max : moment(currentMonth).add(3, 'months')
});

$.calendarWidget.setViews(widget.createCalendar());

// To handle the click event, set the listener to the parent View.
$.calendarWidget.addEventListener('click', function(e) {
	// You can get selectedDate. (moment object)
	var selectedDate = widget.selectedDate();

	Ti.API.info('SelectedDate: ' + selectedDate);
	if (selectedDate != null) {
		Alloy.Globals.contentView.removeAllChildren();
		var dateBooking = Alloy.createController('datebooking', {
			selected_date : selectedDate,
			service : args.service,
			bussiness : args.bussiness
		});
		Alloy.Globals.contentView.add(dateBooking.getView());
	}
});
