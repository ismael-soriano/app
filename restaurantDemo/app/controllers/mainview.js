Ti.API.info(Alloy.Globals.serverData);

var services = Alloy.Globals.serverData.serveis,
    bussiness = Alloy.Globals.serverData.empreses;

if (services !== null) {
	for (var i = 0; i < services.length; i++) {
		Ti.API.info(services[i].nom);

		var serviceOwner;
		for (var j = 0; j < bussiness.length; j++) {
			if (bussiness[j].id == services[i].idCompany)
				serviceOwner = bussiness[j];
		}

		$.services_scrollView.add(createServiceView({
			service : services[i],
			bussiness : serviceOwner,
			serviceText : services[i].nom,
			bussinessText : serviceOwner.nom
		}));

	}
}

function createServiceView(arguments) {
	if (arguments.service === null || arguments.service === undefined)
		arguments.service = '-';

	if (arguments.bussiness === null || arguments.bussiness === undefined)
		arguments.bussiness = '-';

	var view = Ti.UI.createView({
		layout : 'vertical',
		backgroundColor : ISCL.Colors.silver,
		height : '44dp',
		borderColor : ISCL.Colors.blue_gray,
		borderWidth : '1dp',
		service : arguments.service,
		bussiness : arguments.bussiness
	});

	view.add(Ti.UI.createLabel({
		text : arguments.service.nom,
		height : Ti.UI.SIZE,
		// top : 20
	}));

	view.add(Ti.UI.createLabel({
		text : arguments.bussiness.nom,
		height : Ti.UI.SIZE
	}));

	view.addEventListener('click', function() {
		var calendar = Alloy.createController('service_details', {
			service : view.service,
			bussiness : view.bussiness,
			parent : $.mainview
		});
		Alloy.Globals.contentView.removeAllChildren();
		Ti.App.fireEvent('changeLabel:reference', {
			text : 'Detalles del servicio'
		});
		Alloy.Globals.contentView.add(calendar.getView());
	});

	return view;
}

