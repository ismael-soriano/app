(function() {
	function login() {
		if ($.username.value !== "" && $.password.value !== "") {
			hideUserInputElements();
			ISCL.Utils.doLogin($.username.value, Ti.Utils.md5HexDigest($.password.value), {username : $.username.value}, _callBack);
		} else {
			ISCL.UI.showAlertDialog('¡Atención!', 'Debes rellenar todos los campos', 'Ok');
		}
	}

	function _callBack(_params, response) {
		if (!ISCL.Utils.isResponseOk(response))
			showUserInputElements();

		try {
			if (ISCL.Utils.checkResponseError(response) !== 0) {
				Ti.API.info('error iniciar sesión');
				ISCL.Utils.throwError('Ha ocurrido un error al iniciar sesión. Por favor, verifica tus datos.');
			}

			var jsonTmp = ISCL.Utils.parseJson(response);
			if (jsonTmp.rc !== 0) {
				ISCL.Utils.throwError(jsonTmp.info);
			}

			changeDataElements(jsonTmp, _params);
			ISCL.UI.showAlertDialog('Información', 'Datos correctos. Gracias.', 'Aceptar');
			loadUserBookings();
			loadMainView();
		} catch(e) {
			Ti.API.info(e.message);
			showUserInputElements();
			ISCL.UI.showAlertDialog('Error', e.message, 'Aceptar');
		}
	}
	
	function loadUserBookings() {
		var bookings = Alloy.Globals.serverData.reserves;
		Alloy.Globals.userBookings = [];
		
		for (var i=0; i<bookings.length; i++)
		if (bookings[i].Usuari === Alloy.Globals.userName)
			Alloy.Globals.userBookings.push(bookings[i]);
	}

	function loadMainView() {
		var mainView = Alloy.createController('mainview').getView();
		Alloy.Globals.contentView.add(mainView);
	}

	function changeDataElements(jsonTmp, _params) {
		Alloy.Globals.idUserEnc = jsonTmp.idEnc;
		Ti.App.Properties.setString('idUserEnc', jsonTmp.idEnc);
		Ti.API.info(jsonTmp);
		Alloy.Globals.userName = _params.username;
		Ti.App.fireEvent('changeLabel:login');
	}

	function hideUserInputElements() {
		$.username.hide();
		$.password.hide();
		$.loginBtn.hide();
		$.loadActInd.setStyle(ISCL.UI.getActivityIndicatorStyle(ISCL.UI.indicatorType.BIG));
		$.loadActInd.show();
		$.backView.show();
	}

	function showUserInputElements() {
		$.username.show();
		$.password.show();
		$.loginBtn.show();
		$.loadActInd.hide();
		$.backView.hide();
	}


	$.loginBtn.addEventListener('click', login);
})();
