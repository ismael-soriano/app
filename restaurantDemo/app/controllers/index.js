Alloy.Globals.contentView = $.ds.contentView;

Ti.App.addEventListener('index:getResources', function() {
	Ti.App.fireEvent('widget:loadingData');
	ISCL.Utils.getData(Alloy.Globals.getResources);
});

if (ISCL.Utils.isConnectionAvailable()) {
   Titanium.API.info('index -> connection present ');
} else {
   Titanium.API.info('index -> no connection ');
}

Ti.App.fireEvent('index:getResources');
$.index.open();