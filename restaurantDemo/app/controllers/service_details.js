var args = arguments[0] || {};

(function() {
	ISCL.UI.changeText($.service_name, args.service.nom);
	ISCL.UI.changeText($.service_owner, 'Proporcionado por ' + args.bussiness.nom);
	$.service_description.value = args.service.desc;

	$.return_button.addEventListener('click', function showMainView() {
		Alloy.Globals.contentView.removeAllChildren();
		Ti.App.fireEvent('changeLabel:reference', {
			text : 'Servicios'
		});
		Alloy.Globals.contentView.add(args.parent);

	});

	$.show_calendar.addEventListener('click', function() {
		var calendar = Alloy.createController('calendar', args);
		Alloy.Globals.contentView.removeAllChildren();
		Ti.App.fireEvent('changeLabel:reference', {
			text : 'Calendario'
		});
		Alloy.Globals.contentView.add(calendar.getView());
	});
})();
