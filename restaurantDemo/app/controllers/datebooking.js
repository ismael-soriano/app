var args = arguments[0] || {};

(function() {
	var moment = require('alloy/moment');
	var booking = 'Booking\nBussiness: ' + args.bussiness.nom + '\nService: ' + args.service.nom + '\nDate: ' + args.selected_date.format('DD MMMM YYYY');
	var bookingData = {
		service : args.service,
		bussiness : args.bussiness,
		date : args.selected_date,
	};
	ISCL.UI.changeText($.booking_date, args.selected_date.format('DD MMMM YYYY'));

	var serviceHours = getServiceHours();
	Ti.API.info(serviceHours);
	createPage(serviceHours);

	function createPage(hours) {
		createMorning(hours);
		createAfternoon(hours);
	}

	function createMorning(hours) {
		if (hours.iniciMati !== null) {
			$.booking_scrollView.add(createLabel('Mañana'));

			var initial = createHourMoment(serviceHours.iniciMati.split(':')),
			    final = createHourMoment(serviceHours.finalMati.split(':'));
			Ti.API.info(initial.format('HH:mm:ss'));
			var duration = bookingData.service.duration.split(':');

			var times = final.diff(initial, 'minutes') / duration[1];
			for (var i = 0; i < times; i++) {
				if (i !== 0)
					initial.add(duration[1], 'minutes');
				final = moment(initial).add(duration[1], 'minutes');

				//if (!isHourPassed(initial)) {
					$.booking_scrollView.add(createHourView({
						initial : initial,
						final : final
					}));
				//}
			}
		}
	}

	function createAfternoon(hours) {
		if (hours.iniciTarda !== null) {
			$.booking_scrollView.add(createLabel('Tarde'));

			var initial = createHourMoment(serviceHours.iniciTarda.split(':')),
			    final = createHourMoment(serviceHours.finalTarda.split(':'));
			Ti.API.info(initial.format('HH:mm:ss'));
			var duration = bookingData.service.duration.split(':');

			var times = final.diff(initial, 'minutes') / duration[1];
			for (var i = 0; i < times; i++) {
				if (i !== 0)
					initial.add(duration[1], 'minutes');
				final = moment(initial).add(duration[1], 'minutes');

				//if (!isHourPassed(initial)) {
					$.booking_scrollView.add(createHourView({
						initial : initial,
						final : final
					}));
				//}
			}
		}
	}

	function isHourPassed(hour) {
		if (hour.format('dddd D MMMM YYYY LTS') < moment().format('dddd D MMMM YYYY LTS'))
			return true;
		return false;
	}

	function createLabel(_text) {
		return Ti.UI.createLabel({
			text : _text,
			width : Titanium.UI.FILL,
			height : "22dp",
			backgroundColor : "brown",
			color : "white",
			textAlign : 'center',
			font : {
				fontSize : 15,
				fontWeight : 'bold'
			}
		});
	}

	function getServiceHours() {
		var horaris = Alloy.Globals.serverData.horari;
		for (var i = 0; i < horaris.length; i++) {
			if (horaris[i].idCompany === args.bussiness.id)
				return horaris[i];
		}

	}

	function createHourMoment(data) {
		var hour = moment(bookingData.date);
		hour.hours(data[0]);
		hour.minutes(data[1]);
		hour.seconds(data[2]);
		return hour;
	}

	function createHourView(args) {
		var hour = args.initial.format('HH:mm') + ' - ' + args.final.format('HH:mm');
		var view = Ti.UI.createView({
			height : '44dp'
		});

		view.add(Ti.UI.createLabel({
			text : hour,
			height : Ti.UI.SIZE,
			initHour : args.initial.format('HH:mm:ss'),
			finalHour : args.final.format('HH:mm:ss')
		}));

		view.addEventListener('click', function() {
			if (Alloy.Globals.userName !== null) {
				var company = bookingData.bussiness.id,
				    service = bookingData.service.id,
				    date = bookingData.date.format('YYYY-MM-DD'),
				    beginningHour = view.children[0].initHour,
				    finalHour = view.children[0].finalHour;

				ISCL.Utils.checkBooking(company, service, date, beginningHour, finalHour, {
					hour : hour,
					view : view
				}, checkReserve);
			} else {
				ISCL.UI.showAlertDialog('No Conectado', 'Debes estar conectado a una cuenta para poder efectuar una reserva', 'Aceptar');
			}
		});

		return view;
	}

	function createDialog(hour) {
		return Titanium.UI.createAlertDialog({
			title : 'Confirmación hora',
			message : 'Quieres reservar hora para las ' + hour,
			buttonNames : ['Confirmar', 'Cancelar'],
			cancel : 1
		});
	}

	function dialogAddEventListener(dialog, _view) {
		dialog.addEventListener('click', function(e) {
			if (e.cancel === e.index || e.cancel === true) {
				return false;
			}
			if (e.index === 0) {
				var usr = Alloy.Globals.userName,
				    company = bookingData.bussiness.id,
				    service = bookingData.service.id,
				    beginningHour = _view.children[0].initHour,
				    exit = _view.children[0].finalHour,
				    date = bookingData.date.format('YYYY-MM-DD');

				Ti.API.info(usr + '\n' + company + '\n' + service + '\n' + beginningHour + '\n' + exit + '\n' + date);

				ISCL.Utils.saveBooking(usr, company, service, beginningHour, exit, date, {
					view : _view
				}, saveReserve);
				Ti.App.fireEvent('widget:loadingData');
			}
		});
	}

	function addBookingDataToGlobalVariable(initHour, finalHour) {
		bookingData.beginningHour = initHour;
		bookingData.exit = finalHour;
		if (Alloy.Globals.userBookings === null)
			Alloy.Globals.userBookings = [];
		Alloy.Globals.userBookings.push(bookingData);

		Ti.API.info(Alloy.Globals.userBookings);
	}

	function checkReserve(_params, response) {
		if (!ISCL.Utils.isResponseOk(response))
			return false;

		try {
			if (ISCL.Utils.checkResponseError(response) !== 0) {
				Ti.API.info('error al recuperar datos');
				ISCL.Utils.throwError('Ha ocurrido un error al descargar los datos del servidor. Por favor, verifica tu conexión.');
			}

			var jsonTmp = ISCL.Utils.parseJson(response);
			if (jsonTmp.rc !== 0) {
				ISCL.Utils.throwError(jsonTmp.info);
			}

			Ti.API.info(jsonTmp);

			Ti.API.info('datebooking -> checkreserve ok');
			var dialog = createDialog(_params.hour);
			dialogAddEventListener(dialog, _params.view);
			dialog.show();
			return true;
		} catch(e) {
			Ti.API.info(e.message);
			ISCL.UI.showAlertDialog('Error', e.message, 'Aceptar');
			return false;
		}
	}

	function saveReserve(_params, response) {
		Ti.API.info(_params.view.children[0]);

		if (!ISCL.Utils.isResponseOk(response)) {
			loadMainView();
			return false;
		}

		try {
			if (ISCL.Utils.checkResponseError(response) !== 0) {
				Ti.API.info('error al recuperar datos');
				ISCL.Utils.throwError('Ha ocurrido un error al descargar los datos del servidor. Por favor, verifica tu conexión.');
			}

			var jsonTmp = ISCL.Utils.parseJson(response);
			if (jsonTmp.rc !== 0) {
				ISCL.Utils.throwError(jsonTmp.info);
			}

			Ti.API.info('datebooking -> booking saved');
			addBookingDataToGlobalVariable(_params.view.children[0].initHour, _params.view.children[0].finalHour);
			ISCL.UI.showAlertDialog('Reserva registrada', booking + '\nHour: ' + _params.view.children[0].text, 'Ok');
			return true;
		} catch(e) {
			Ti.API.info(e.message);
			ISCL.UI.showAlertDialog('Error', e.message, 'Aceptar');
			return false;
		} finally {
			loadMainView();
		}
	}

	function loadMainView() {
		Ti.App.fireEvent('widget:dataLoaded', {
			view : 'mainview'
		});
	}

})();
