// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};

// Import the Compatibility Layer API.
require('CompatibilityAPI');

// Colors
Alloy.Globals.darkColor = "#420006";
Alloy.Globals.lightColor = "#8a3239";

// Measures
Alloy.Globals.screenWidth = ISCL.UI.getScreenWidth();
Alloy.Globals.screenHeight = ISCL.UI.getScreenHeight();
Alloy.Globals.seventyfive_percent_width = Titanium.Platform.displayCaps.platformWidth * 0.75;
Alloy.Globals.twentyfive_percent_width = Titanium.Platform.displayCaps.platformWidth * 0.25;

// Phone Status
Alloy.Globals.onPortrait = true;
Alloy.Globals.onLandscape = false;

// Loged
Alloy.Globals.login = false;
Alloy.Globals.userName = null;

// View on to load the items
Alloy.Globals.contentView = null;

// Bulk of data
Alloy.Globals.serverData = null;
Alloy.Globals.userBookings = null;

// Server Global Values
Alloy.Globals.serverURL = "http://192.168.1.150/salestest/php/";
Alloy.Globals.loginService = "login.php";
Alloy.Globals.getDataService = "getData.php";
Alloy.Globals.doBookingService = "saveReserve.php";
Alloy.Globals.checkBookingService = "checkReserve.php";

Alloy.Globals.idENCUser = "servicesclient";
Alloy.Globals.idAppEnc = "servicesdemo";
Alloy.Globals.version = "1.0";
Alloy.Globals.serverKey = "loquequieras";

Alloy.Globals.DATA_STATUS__NOT_INITIALIZED = 0;
Alloy.Globals.DATA_STATUS__DOWNLOADING = 2;
Alloy.Globals.DATA_STATUS__DOWNLOADED = 3;
Alloy.Globals.DATA_STATUS__ERROR = 4;
Alloy.Globals.dataStatus = Alloy.Globals.DATA_STATUS__NOT_INITIALIZED;

Alloy.Globals.getResources = function(response) {
	if (!ISCL.Utils.isResponseOk(response))
		return false;

	try {
		if (ISCL.Utils.checkResponseError(response) !== 0) {
			Ti.API.info('error al recuperar datos');
			ISCL.Utils.throwError('Ha ocurrido un error al descargar los datos del servidor. Por favor, verifica tu conexión.');
		}

		var jsonTmp = ISCL.Utils.parseJson(response);
		if (jsonTmp.rc !== 0) {
			ISCL.Utils.throwError(jsonTmp.info);
		}
		
		Ti.API.info(jsonTmp.data);
		Alloy.Globals.idUserEnc = jsonTmp.idEnc;
		Ti.App.Properties.setString('idUserEnc', jsonTmp.idEnc);
		Alloy.Globals.serverData = jsonTmp.data;

		Ti.API.info('Alloy -> Done Loading data');
		Ti.API.info('Alloy -> serverData: ' + Alloy.Globals.serverData);
		Ti.App.fireEvent('widget:dataLoaded', {
			view : 'mainview'
		});
		return true;
	} catch(e) {
		Ti.API.info(e.message);
		ISCL.UI.showAlertDialog('Error', e.message, 'Aceptar');
		return false;
	}
}; 