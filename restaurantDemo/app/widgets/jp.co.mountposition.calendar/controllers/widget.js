var args = arguments[0] || {};

(function() {
	var moment = require('alloy/moment'),
		screenWidth = ISCL.UI.getScreenWidth(),
		screenHeight = ISCL.UI.getScreenHeight(),
	    TILE_WIDTH,
	    TILE_HEIGHT,
	    _j,
	    _k,
	    _ref1,
	    _ref2,
	    _len,
	    day,
	    tile,
	    _,
	    selected = null;

	var init = moment(args.min);
	var final = moment(args.max);
	var duration = moment.duration(final.diff(init));
	var months = Math.floor(duration.asMonths());

	Ti.API.info('Calendar -> months: ' + months);

	exports.createCalendar = function() {
		var start = new Date().getTime();
		
		var views = [];
		var calendarWidget,
		    labelMonth,
		    days,
		    dates;

		for (var i = 0; i < months; i++) {
			calendarWidget = Ti.UI.createView({
				backgroundRepeat : true,
				backgroundImage : WPATH('/images/bg.png'),
				width : '100%',
				height : '100%',
				layout : 'vertical'
			});

			labelMonth = Ti.UI.createLabel({
				height : '40dp',
				width : 'auto',
				textAlign : 'center',
				color : "#000"
			});

			days = Ti.UI.createView({
				backgroundRepeat : true,
				layout : 'horizontal',
				height : '22dp',
			});

			dates = Ti.UI.createView({
				layout : 'vertical',
				height : Titanium.UI.FILL,
			});

			Ti.API.info('Creating calendar');

			// Sets the period for the calendar.
			var period = moment(init).add(i, 'months'),
			    availableDates = args.availableDates;

			// Color definitions for the tiles of the calendar.
			var WEEK_COLOR = ['#FF9999', '#999999', '#999999', '#999999', '#999999', '#999999', '#91C176'],
			    DAY_COLOR = ['#FF0000', '#333333', '#333333', '#333333', '#333333', '#333333', '#64A515'],
			    OUTDAY_COLOR = ['#FF9999', '#999999', '#999999', '#999999', '#999999', '#999999', '#91C176'],
			    TODAY_COLOR = ['#FF9999', '#999999', '#999999', '#999999', '#999999', '#999999', '#91C176'];

			// Tile dimensions
			exports.TILE_WIDTH = TILE_WIDTH = Math.floor(screenWidth/7);
			exports.TILE_HEIGHT = TILE_HEIGHT = Math.floor(100 / 6) + "%";

			// Width of the calendar
			var CALENDAR_WIDTH = Ti.UI.FILL;

			// Same width for the rows of dates and days
			//$.days.width = $.dates.width = CALENDAR_WIDTH;

			// Set the label for the month and Year
			ISCL.UI.changeText(labelMonth, period.format('MMMM YYYY'));

			// Variable for the selected day
			selected = null;

			// Main click function of the calendar
			dates.addEventListener('click', function(e) {
				Ti.API.info('Calendar clicked');
				Ti.API.log('Calendar', e.source.children[0]);
				if ((e.source.date !== null) && !e.source._isEntry) {
					selected = e.source;
				}
				Ti.API.info('Calendar -> selected: ' + selected);
			});

			// Calendar Header
			var daysEnum = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
			for (var j = 0,
			    _len = daysEnum.length; j < _len; j++) {
				day = daysEnum[j];

				var contents = {
					color : WEEK_COLOR[j],
					textAlign : 'center',
					font : {},
					text : day,
					width : TILE_WIDTH,
					height : 'auto',
				};

				if (j == 0)
					contents.left = '1%';

				days.add(Ti.UI.createLabel(contents));
			}

			calendarWidget.calendar = {};

			// Actual Month
			var calendarMonth = moment(period);

			period.date(1);

			var dayOfWeek = period.day(),
			    prevMonth = moment(period).subtract('months', 1),
			    nextMonth = moment(period).add('months', 1);

			var col = 0,
			    row = 0;

			var createWeekView = function() {
				return Ti.UI.createView({
					layout : 'horizontal',
					height : TILE_HEIGHT,
					center : true,
				});
			};

			var weekView = createWeekView();
			// Ti.API.info('weekview width: ' + weekView.getWidth());

			/**
			 * Function that creates the labels for the days of the calendar.<br><br>
			 *
			 * It recieves as a parameter the color for the day and a value v
			 * containing if its available or unavailable.<br><br>
			 *
			 * Usage: createLabel(text, width, dayColor, availability, prevMonth, nextMonth);
			 *
			 * @param {String} text
			 * @param {Object} width
			 * @param {Object} dayColor - Color for the text on the label.
			 * @param {String} availability - String containing one of this 3 values: actual,
			 * 									available or unavailabe.
			 * @param {boolean} prevMonth
			 * @param {boolean} nextMonth
			 */
			var createLabel = function(contents) {
				var background,
				    isAvailable;

				if (contents.text === null && contents.text === undefined)
					contents.text = '';

				if (contents.width === null && contents.width === undefined)
					contents.width = Ti.UI.SIZE;

				if (contents.dayColor === null && contents.dayColor === undefined)
					contents.dayColor = '#333333';

				if (contents.availabilty === null && contents.availabilty === undefined)
					contents.availability = "unavailable";

				if (contents.prevmonth === null && contents.prevmonth === undefined)
					contents.prevMonth = false;

				if (contents.nextMonth === null && contents.nextMonth === undefined)
					contents.nextMonth = false;

				if (contents.left === null && contents.left === undefined)
					contents.left = '0%';

				if (contents.date === null && contents.date === undefined)
					contents.date = '';

				if (contents.availability == "actual") {
					background = WPATH('/images/calendar/selected.png');
					isAvailable = true;
				} else if (contents.availability == "unavailable") {
					background = WPATH('/images/calendar/inactive.png');
					isAvailable = false;
				} else {
					background = WPATH('/images/calendar/tile.png');
					isAvailable = true;
				}

				return Ti.UI.createLabel({
					left : contents.left,
					color : contents.dayColor,
					backgroundImage : background,
					font : {},
					textAlign : 'center',
					text : contents.text,
					width : contents.width,
					height : Ti.UI.FILL,
					_isEntry : false,
					touchEnabled : false,
					available : isAvailable,
					prevMonth : contents.prevMonth,
					nextMonth : contents.nextMonth,
					date : contents.date
				});
			};

			// Create days of the past Month
			if (dayOfWeek !== 0) {
				for (var j = _j = _ref1 = dayOfWeek - 1; (_ref1 <= 0) ? (_j <= 0) : _j >= 0; j = (_ref1 <= 0) ? ++_j : --_j) {
					var contents = {
						text : prevMonth.daysInMonth() - j,
						width : TILE_WIDTH,
						dayColor : OUTDAY_COLOR[col],
						availability : 'unavailable',
						prevMonth : true
					};

					if (col == 0)
						contents.left = '1%';

					weekView.add(createLabel(contents));
					col++;
				}
			}

			// Create the labels for the Month.
			for (var j = _k = 1,
			    _ref2 = period.daysInMonth(); (1 <= _ref2) ? (_k <= _ref2) : (_k >= _ref2); j = (1 <= _ref2) ? ++_k : --_k) {
				var viewContents = {
					backgroundColor : 'transparent',
					width : TILE_WIDTH,
					height : Ti.UI.FILL,
					date : period.unix()
				};
				
				if (col == 0)
					viewContents.left = '1%';
				
				tile = Ti.UI.createView(viewContents);
				//Ti.API.info("i: " + i + ", _k: " + _k + ", _ref2: " + _ref2);

				var contents = {
					text : period.date(),
					width : Ti.UI.FILL,
					dayColor : DAY_COLOR[col]
				};

				if (period.format('L') == moment().format('L')) {
					contents.availability = "actual";
					tile.add(createLabel(contents));
				} else if (period.format('L') < moment().format('L')) {
					contents.availability = 'unavailable';
					tile.add(createLabel(contents));
				} else {
					tile.add(createLabel(contents));
				}

				weekView.add(tile);
				//$.calendar["" + period.date()] = tile;
				period.add(1, 'days');
				col++;
				if (col === 7) {
					dates.add(weekView);
					weekView = createWeekView();
					col = 0;
					row++;
				}
			}

			// Ti.API.info('Calendar -> row: ' + row);
			// Create the labels for the days of the next month
			var times = 6 - row;

			// Ti.API.info('times : ' + times);
			for (var j = 0; j < times; j++) {
				do {
					var contents = {
						text : nextMonth.date(),
						width : TILE_WIDTH,
						dayColor : OUTDAY_COLOR[col],
						availability : 'unavailable',
						nextMonth : 'true'
					};

					if (col == 0)
						contents.left = '1%';

					weekView.add(createLabel(contents));
					nextMonth.add('days', 1);
					col++;
					if (col === 7) {
						dates.add(weekView);
						col = 0;
						row++;

						if (row < 6)
							weekView = createWeekView();
					}
					// Ti.API.info('row: ' + row + ', Col: ' + col);
				} while (col !== 0);
			}

			// Ti.API.info('Calendar -> row: ' + row);
			Ti.API.info('Calendar created');

			calendarWidget.add(labelMonth);
			calendarWidget.add(days);
			calendarWidget.add(dates);
			views.push(calendarWidget);
		}

		for (var i = 0; i < views.length; i++)
			Ti.API.info('Calendar -> view ' + i + ': ' + views[i].children[0].text);

		var end = new Date().getTime();
		Ti.API.info('elapsed ms: ' + (end - start));
		
		return views;
	};

	function rowsNumber() {
		var dayOfWeek = period.day(),
		    prevMonth = moment(period).subtract('months', 1),
		    nextMonth = moment(period).add('months', 1);

		var row = 0,
		    col = 0;

	}

	/**
	 * Sets an image for the specified date on the calendar.
	 */
	exports.setImage = function(day, image, options) {
		var _ref3;
		if (options === null) {
			options = {};
		}
		if (moment.isMoment(day)) {
			day = day.date();
		}
		tile = ( _ref3 = calendarWidget.calendar) !== null ? _ref3["" + day] :
		void 0;
		if ((tile !== null ? tile.date :
		void 0) !== null) {
			tile.remove(tile.children[0]);
			_.extend(tile, {
				_isEntry : true
			}, options);
			return tile.add(Ti.UI.createImageView({
				image : image,
				width : TILE_WIDTH,
				height : TILE_HEIGHT, //TILE_WIDTH,
				touchEnabled : false
			}));
		}
	};

	exports.setView = function(day, view, options) {
		var _ref3;
		if (options === null) {
			options = {};
		}
		if (moment.isMoment(day)) {
			day = day.date();
		}
		tile = ( _ref3 = calendarWidget.calendar) !== null ? _ref3["" + day] :
		void 0;
		if (tile !== null) {
			_.extend(tile, options);
			return tile.add(view);
		}
	};

	/**
	 * Returns the month of the Calendar.
	 */
	exports.calendarMonth = function() {
		return calendarMonth;
	};

	/**
	 * Selects a day from the calendar, using this function would trigger the
	 * doClick() for the day selected of the calendar.
	 *
	 * @param {object} day
	 */
	exports.select = function(day) {
		var touchEvent,
		    _ref3;
		if (moment.isMoment(day)) {
			day = day.date();
		}
		touchEvent = OS_ANDROID ? 'singletap' : 'click';
		tile = ( _ref3 = calendarWidget.calendar) !== null ? _ref3["" + day] :
		void 0;
		return tile !== null ? tile.fireEvent(touchEvent, {
			source : tile
		}) :
		void 0;
	};

	/**
	 *  Returns the selected date from the calendar.
	 */
	exports.selectedDate = function() {
		Ti.API.info(selected);
		if (selected !== null && selected.children[0].available === true) {
			Ti.API.info('Calendar -> selected: ' + moment.unix(selected.date).format('DD MMMM YYYY'));
			return moment.unix(selected.date);
		} else {
			Ti.API.info('Calendar -> SelectedDate Null');
			return null;
		}
	};

	/**
	 * The day passed as a reference to this function would become unavailable,
	 * in this sense, the day couldn't be selected and accessed through the
	 * calendar.
	 *
	 * When done, the image for this day would change to 'unavailable.png'.
	 *
	 * @param {Object} day
	 */
	exports.setDayUnavailable = function(day) {
		var touchEvent,
		    _ref3;
		if (moment.isMoment(day)) {
			day = day.date();
		}

		tile = ( _ref3 = calendarWidget.calendar) !== null ? _ref3["" + day] :
		void 0;
		if (tile !== null) {
			tile.children[0].backgroundImage = WPATH('/images/calendar/inactive.png');
			tile.children[0].available = "false";
		}
	};

	/**
	 * Destroys the calendar and sets to null the value of the selected day.
	 */
	exports.destroy = function() {
		calendarWidget.calendar = null;
		calendarWidget.selected = null;
		return calendarWidget.destroy();
	};

})();
