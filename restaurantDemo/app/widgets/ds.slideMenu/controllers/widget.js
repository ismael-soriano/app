// Vars
var buttonPressed = false;
var hasSlided = false;
var direction = "reset";
var moving = false;
var axis;

//*********** Animations ***************/
var animateOpen = Ti.UI.createAnimation({
	left : "250dp",
	curve : Ti.UI.ANIMATION_CURVE_EASE_OUT,
	duration : 150
});

var animateClose = Ti.UI.createAnimation({
	left : "0dp",
	curve : Ti.UI.ANIMATION_CURVE_EASE_OUT,
	duration : 150
});

$.widgetView.addEventListener('swipe', function(e) {
	if (e.direction == 'left' && hasSlided) {
		$.mainView.animate(animateClose);
		hasSlided = false;
	}	
});

$.menuButton.addEventListener('click', function(e) {
	$.toggleLeftSlider();
});

//*************** Functions *******************/
exports.toggleLeftSlider = function() {
	if (!hasSlided) {
		direction = "right";
		$.mainView.animate(animateOpen);
		hasSlided = true;
	} else {
		direction = "reset";
		$.menuButton.touchEnabled = true;
		$.mainView.animate(animateClose);
		hasSlided = false;
	}
};

function toggleView(v) {
	$.contentView.removeAllChildren();
	var currentView = Alloy.createController(v).getView();
	$.contentView.add(currentView);
	$.toggleLeftSlider();
}

function showDoBooking(e) {
	Ti.App.fireEvent('changeLabel:reference', {
		text : 'Efectuar Reserva'
	});
	Ti.App.fireEvent('index:getResources');
	$.toggleLeftSlider();
}

function showBooking(e) {
	toggleView('booking');
	Ti.App.fireEvent('changeLabel:reference', {
		text : 'Ver Reserva'
	});
}

function showContact(e) {
	toggleView('contact');
	Ti.App.fireEvent('changeLabel:reference', {
		text : 'Contacto'
	});
}

function showInfo(e) {
	toggleView('info');
	Ti.App.fireEvent('changeLabel:reference', {
		text : 'Información y comentarios'
	});
}

var doLogin = function() {
	if (Alloy.Globals.login == false) {
		toggleView('login');
		Ti.App.fireEvent('changeLabel:reference', {
			text : 'Conectar'
		});
	}
};

var logOut = function() {
	$.login_title.text = "Conectar";
	$.login_view.removeEventListener('click', logOut);
	$.login_view.addEventListener('click', doLogin);
	Alloy.Globals.login = false;
	$.toggleLeftSlider();
};


//************* Event Listeners ***********************/

// Change Label : login
Ti.App.addEventListener('changeLabel:login', function() {
	ISCL.UI.changeText($.login_title, 'Desconectar');

	Alloy.Globals.login = true;
	$.login_view.removeEventListener('click', doLogin);
	$.login_view.addEventListener('click', logOut);
});

// Change Label : reference
Ti.App.addEventListener('changeLabel:reference', function(data) {
	ISCL.UI.changeText($.referenceLabel, data.text);	
});

// Open Menu
Ti.App.addEventListener('openMenu', function() {
	$.toggleLeftSlider();
});

// Loading Data
Ti.App.addEventListener('widget:loadingData', function() {
	$.dataStatus.show();
	$.contentView.removeAllChildren();
	$.contentView.add(Ti.UI.createView({
		backgroundColor : 'white'
	}));
});

// Data Loaded
Ti.App.addEventListener('widget:dataLoaded', function(args) {
	$.dataStatus.hide();
	$.contentView.removeAllChildren();
	var currentView = Alloy.createController(args.view).getView();
	$.contentView.add(currentView);
});

// Data Error
Ti.App.addEventListener('widget:dataError', function() {
	$.dataStatus.hide();
	$.contentView.removeAllChildren();
	var currentView = ISCL.UI.createConectionErrorView('No se ha podido acceder al servidor');
	$.contentView.add(currentView);
});


//********** Initialize the elements *****************/
ISCL.UI.changeText($.login_title, 'Conectar');
$.login_view.addEventListener('click', doLogin);
Ti.App.fireEvent('changeLabel:reference', {
	text : 'Pruebas'
});
